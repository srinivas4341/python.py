#!/usr/bin/env python
# coding: utf-8

# In[1]:


x = 0

while x < 5:
    print(f'The current value of x is {x}')
    x += 1
else:
    print("x IS NOT LESS THAN 5")


# In[2]:


x = [1,2,3]

for item in x:
    # comment
    pass 

print('end of my script')


# In[7]:


mystring = 'srinu'


# In[8]:


for letter in mystring:
    if letter == 'r':
        break
    print(letter)


# In[9]:


x = 0

while x < 5:
    
    if x == 2:
        break
    print(x)
    x += 1


# In[1]:


mylist = [1,2,3]


# In[2]:


for num in range(10):
    print(num)


# In[3]:


range(0,11,2)


# In[7]:


for num in range(0,11,2):
    print(num)


# In[8]:


list(range(0,11,2))


# In[9]:


index_count = 0

for letter in 'abcde':
    print('At index {} the letter is {}'.format(index_count,letter))
    index_count += 1


# In[12]:


index_count = 0
word = 'abcde'

for index,letter in enumerate(word):
    print(index)
    print(letter)
    print('\n')
    


# In[17]:


mylist1 = [1,2,3]
mylist2 = ['a','b','c']


# In[18]:


zip(mylist1,mylist2)


# In[19]:


from random import shuffle


# In[20]:


mylist = [1,2,3,4,5,6,7,8,9,10]


# In[21]:


shuffle(mylist)


# In[22]:


mylist


# In[23]:


input('Enter a number here: ')


# In[ ]:




