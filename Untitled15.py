#!/usr/bin/env python
# coding: utf-8

# In[1]:


a=5
b=10


# In[5]:


print(a+b)


# In[3]:


a=5
b=10


# In[4]:


print(a+b)


# In[6]:


a='aadharsh'


# In[7]:


print(a)


# In[9]:


a="aadharsh"


# In[10]:


print(a)


# In[12]:


a='i'
a='am'
a='srinivas'


# In[13]:


print(a)


# In[14]:


a='i am srinivas'


# In[15]:


print(type(a))


# In[17]:


a=True


# In[18]:


print(type(a))


# In[19]:


a=2.8


# In[20]:


print(int(a))


# In[21]:


print(float(a))


# In[22]:


print(str(a))


# In[24]:


a='457'


# In[25]:


print(int(a))


# In[26]:


a=457


# In[27]:


print(str(a))


# In[28]:


a='aadhi'


# In[29]:


print(int(a))


# In[30]:


a='True'


# In[31]:


print(type(a))


# In[32]:


a=3
b=4


# In[33]:


print(a+b)


# In[34]:


print(a-b)


# In[35]:


print(a*b)


# In[36]:


print(a/b)


# In[37]:


print(a//b)


# In[38]:


print(a%b)


# In[39]:


print(a**b)


# In[40]:


10<40


# In[41]:


10>40


# In[42]:


10<=40


# In[45]:


10==40


# In[46]:


10>1010<4020


# In[59]:


a=10
b=30


# In[61]:


not(a>b)


# In[65]:


10<20 and 10==10


# In[69]:


10<20 or 5==10


# In[70]:


a=20
b=50


# In[71]:


not(a<b)


# In[73]:


a=3
a+=3


# In[74]:


print(a)


# In[75]:


a=10
a-=3


# In[76]:


print(a)


# In[77]:


a=10
a*=3


# In[78]:


print(a)


# In[79]:


a=10
a/=3


# In[80]:


print(a)


# In[87]:


word = "solution"
another_word = "another solution"
third_word = "3rd solution"


# In[91]:


all_words = word + " " + another_word + " " + third_word


# In[92]:


all_words


# In[96]:


name1 = 'srinu'
name2 = 'sai'
name1!=name2


# In[97]:


a=10
a<<1


# In[98]:


a=10
a<<2


# In[99]:


a=10
a>>1


# In[100]:


a=13
a>>1


# In[101]:


a=13
a>>2


# In[105]:


a=10
b=10
print(a==b)


# In[106]:


print(id(a),id(b))


# In[107]:


a!=b


# In[110]:


a='yesterday is a holiday'
print('aadhi'not in a)


# In[142]:


if 10<20:
elif 10>20:
print("10 is less than 20")
    


# In[143]:


if 10<20:
    print(True)
else:
    print(False)


# In[145]:


if 10>20:
    print(10)
elif 10<20:
    print(20)elif
else:("equal") if


# In[152]:


if 20<40:
    print(20)
elif 20<50:
    print(20)
else:
    print("Equal")


# In[ ]:





# In[169]:


if 10>=20:
    if 10>20:
        print(10)
    else:
        print("equal")
else:
    print("10 is less")


# In[171]:


if 10>20:
    print(10)
elif 10<20:
    print(20)
else:("equal")


# In[174]:


10==10


# In[ ]:




